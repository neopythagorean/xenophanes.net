

# Head

![](../static/images/headref-small.png)

Flavius's head will be created by [SpiltGrapeSoda](https://www.spiltgrapesoda.com/).

I can't wait omg...

# Tail

My fursuit's tail is based on [this](https://www.etsy.com/listing/975540385/pdf-pattern-tail-pattern-pack-4) pattern by FurSmoothie.
I used the deer pattern from this collection, ignoring the top/bottom instructions.

This pattern is quite simple, and the instructions are great.

I really wish I just used hot glue rather than the 3M, although I think it turned out fine regardless.

# Hooves

My hoofers are a slighly modified version of [this](https://www.etsy.com/listing/836126730/puffy-hand-hoof-fursuit-pattern-cloven) pattern by ObleStudios.

You don't need to go heavy on the glue.
On the right hoof thumb I put too much glue between the minky and EVA foam, and this lead to the minky being kinda skrunkly.
I don't think it's a huge problem, and I avoided this mistake on the fingers and other thumb.
If I were to make these again I would use hot glue instead.

# Materials

These were the fabrics used for all the parts:
 * Wool: [Faux Fur Long Pile MONGOLIAN SHEEP LIGHT CAMEL Fabric](https://www.fabricempire.com/faux-fur-long-pile-mongolian-sheep-light-camel-fabric-64-w-sold-by-the-yard.aspx)
 * Main Fur: [Charcoal Grey Luxury Teddy Faux Fur](https://www.howlfabrics.com/product-page/charcoal-grey-luxury-teddy-faux-fur) 
 * Muzzle Fur: [Ivory Solid Shaggy Long Pile Fabric](https://bigzfabric.com/index.php/faux-fake-fur-solid-shaggy-long-pile-fabric-ivory-sold-by-the-yard-ecoshagtm.html)
 * Hooves / Inner Ear: [Pewter Minky Cuddle Solid Fabric](https://www.howlfabrics.com/product-page/pewter-minky-cuddle-solid-fabric)
 * Nose: [Silver Minky Cuddle Solid Fabric](https://www.howlfabrics.com/product-page/silver-minky-cuddle-solid-fabric)
 * Horns: [Ivory Minky Cuddle Solid Fabric](https://www.howlfabrics.com/product-page/ivory-minky-cuddle-solid-fabric)
 * Tongue: [Paris Pink Minky Cuddle Solid Fabric](https://www.howlfabrics.com/product-page/uv-paris-pink-minky-cuddle-solid-fabric)
 * Hoof Lining: Some random white muslin I had lying around from walmart
 * Tail Backing: One sheet of black craft felt, from walmart
 * Stuffing: Polyfill, from walmart
 * Padding: [EVA Foam](https://www.amazon.com/dp/B089K9769X)
 * Glue: 3M General Trim Adhesive, from walmart
 * Thread: Assorted polyester thread, from walmart


# Equipment

## Sewing Machine

Currently, my "sewing machine" is a [Singer M1000](https://www.singer.com/m1000-mending-machine-0).
Sewing machine is in quotes, as I do not think this machine was ever meant to be used as a serious sewing machine.
Indeed, I cannot be harsh on it.
Singer advertises it as a mending machine, meant to be light and portable for when you just need to do repairs.

Perhaps my biggest issue with this machine is the fact that it is one speed.
The foot pedal is a digital switch, it is either completely on or off.
This can make controlling the machine quite frustrating.
I think the only saving grace of this is that the on speed is quite slow compared to a normal sewing machine.



