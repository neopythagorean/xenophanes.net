
let () =
  Dream.run
  @@ Dream.logger
  @@ Dream.router [ Dream.get "/static/images/:img" Images.get_image
                  ; Dream.get "/static/**" (Dream.static "./static")
                  ; Dream.get "/" ( fun _ -> Dream.html Pages.home )
                  ; Dream.get "/about" (fun _ -> Dream.html Pages.about)
                  ; Dream.get "/fursona" (fun _ -> Dream.html Pages.fursona)
                  ; Dream.get "/findme" (fun _ -> Dream.html Pages.socials)
                  ; Dream.get "/blog/" (fun _ -> Dream.html Pages.blog_main)
                  ; Dream.get "/blog" (fun _ -> Dream.html Pages.blog_main)
                  ; Dream.get "/blog/:entry" (fun r -> Dream.html @@ Pages.blog_page @@ Dream.param r "entry") ]



