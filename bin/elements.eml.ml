
let (<<) f g x = f (g x)

let html_wrapper head body = 
  <!DOCTYPE html>
  <html lang="en">
    <head>
      <%s! head %>
    </head>
    <body class="body">
      <%s! body %>
    </body>
  </html>

let ascii text =
  <pre class="ascii" aria-hidden="true">
  <%s text%>
  </pre>

let normal_head title = 
  <meta charset="utf-8">
  <link rel="stylesheet" href="/static/main.css">
  <link rel="stylesheet" href="https://furryring.neocities.org/onionring.css">
  <title> <%s title %> </title>

let page = html_wrapper << normal_head

let bold i = String.trim @@
  <strong><%s! i %></strong>

let link loc i = String.trim @@
  <a href="<%s loc %>"><%s! i%></a>
 
let p i = String.trim @@
  <p>
    <%s! i %>
  </p>

let mono i = String.trim @@
  <div class="mono">
  <%s! i %>
  </div>

let twitter at = String.trim @@ link ("https://twitter.com/" ^ at) ("@" ^ at)

let center i = String.trim @@
    <div class="center">
      <%s! i %>
    </div>

let image loc width height =
  <img src="<%s loc %>" width="<%s width %>" height="<%s height %>">

let fit t =
  <span class="text-fit">
  <span><span><%s! t  %></span></span>
  <span aria-hidden="true"><%s! t %></span>
  </span>

let fita loc i =
  <span class="text-fit">
  <span><span><%s! link loc i  %></span></span>
  <span aria-hidden="true"><%s! i %></span>
  </span>

let md = Omd.to_html << Omd.of_string
let md_file = Omd.to_html << Omd.of_channel << open_in
