let read_file f =
  let ch = open_in_bin f in
  let s = really_input_string ch (in_channel_length ch) in
  close_in ch;
  s


