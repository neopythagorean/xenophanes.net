open Elements

let figlet = "__
\\ \\__                  _
 > __)                | |
( (_  ___ _  _____   _| |_   __  ___  ___ __   ____
 > _)/ __) |/ / _ \\ /     \\ /  \\/ / |/ / '_ \\ / ___)
( (__> _)| / ( (_) | (| |) | ()  <| / /| | | ( (__
 \\__ \\___)__/ \\___/ \\_   _/ \\__/\\_\\__/ |_| | |\\__ \\
    ) )               | |                  | |  _) )
   (_/                |_|                  |_| (__/"

let p404 = " _  _    ___  _  _
| || |  / _ \\| || |
| || |_| | | | || |_
|__   _| |_| |__   _|
   |_|  \\___/   |_|"

let disclaimer = String.trim @@ p @@
  %%
  Made in <%s! link "https://ocaml.org" "OCaml" %> with <%s! link "https://aantron.github.io/dream" "Dream"%>. <%s! link "https://gitlab.com/neopythagorean/xenophanes.net" "website source" %>.
  %%

let furryring =
  <div id='furryring'>
  <script type="text/javascript" src="https://furryring.neocities.org/onionring-variables.js"></script>
  <script type="text/javascript" src="https://furryring.neocities.org/onionring-widget.js"></script>
  </div>

let home = page "ξενοφάνης" @@ mono @@
  <%s! ascii figlet %>
  <%s! p "Ron's web portal" %>
  <%s! p @@ link "blog" "blog / opinions on things / et cetera" %>
  <%s! p @@ link "fursona" "my fursona ^w^" %>
  <%s! p @@ link "findme" "socials, et cetera" %>
  <%s! disclaimer %>
  <%s! furryring %>

let about =
  page "about" @@
    mono "about me :3c"


let fs1 = "\
this is my fursona **flavius** :3c.
full name: [flavius claudius julianus](https://en.wikipedia.org/wiki/Julian_(emperor)).
he is a shetland sheep 🐑, and he is literally me.
despite the roman name i get art of him however i like and in whatever era i please and there is nothing you can do about it.
"

(*type furry_art = {loc : string; artist : string; desc : string};*)

let fursona = page "flavius" @@ mono @@
  (center @@ link "static/images/jpegdisco.png" @@ image "static/images/jpegdisco-small.png" "300" "310") ^ md fs1 ^ "art by " ^ twitter "jpegdisco"

let blog_date e = Yojson.Basic.Util.to_string @@ Yojson.Basic.Util.member "date" @@ e
let blog_desc e = Yojson.Basic.Util.to_string @@ Yojson.Basic.Util.member "desc" @@ e
let blog_link e = Yojson.Basic.Util.to_string @@ Yojson.Basic.Util.member "link" @@ e

let blog_entry e = p @@
  <%s blog_date e %> <%s! link ("/blog/" ^ blog_link e) (blog_desc e) %>

let footer = p @@
  <%s! link "/" "ξενοφάνης" %>

let blog_footer = p @@
  <%s! link "/" "ξενοφάνης" %>
  <br>
  <%s! link "/blog" "blog" %>

let socials = 
  page "socials" @@ mono @@
  <%s! fit "LINKS" %>
  <%s! fita "https://xenophanes.net"  "ξενοφάνης" %>
  <%s! fita "https://twitter.com/flavius_ovis"  "twitter" %>
  <%s! fita "https://bsky.app/profile/flavius.bsky.social"  "bluesky" %>
  <%s! fita "https://flaviusovis.tumblr.com"    "tumblr"  %>
  <%s! fita "https://gitlab.com/neopythagorean" "gitlab"  %>
  <%s! fita "https://github.com/neopythagorean" "github"  %>

(*<%s! ascii @@ Ascii.read_file "static/ascii/flavius.txt" %>*)

let blog_main = 
  let entries = Yojson.Basic.Util.to_list @@ Yojson.Basic.from_file "static/blog/entries.json" in
  page "writings" @@ mono @@ ( String.concat "\n" @@ List.map blog_entry @@ entries ) ^ footer

let blog_page l =
  let metadata = Yojson.Basic.from_file @@ "static/blog/" ^ l ^ ".json" in
  let title =  Yojson.Basic.Util.to_string @@ Yojson.Basic.Util.member "title" metadata in
  let date =  Yojson.Basic.Util.to_string @@ Yojson.Basic.Util.member "date" metadata in
    page title @@ mono @@
    (p @@ (bold title) ^ "<br>" ^ date)
    ^ md_file ("static/blog/" ^ l ^ ".md")
    ^ blog_footer



